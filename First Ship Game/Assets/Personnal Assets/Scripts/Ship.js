﻿class Boundary
{
    var yMin : float = -9;
    var yMax : float = 9;
    var zMin : float = -30;
    var zMax : float = 30;
}

var speed : float = 30;
var tilt : float = 1;
var boundary : Boundary;
var bullet : GameObject;
var bulletSpawnLeft : Transform;
var bulletSpawnRight : Transform;
var explosion : GameObject;
var restarter : GameObject;

var weaponType : String = "defaultGun";
var defaultGunShootingSpeed : float = 0.7;
var beamGunShootingSpeed : float = 1.5;
var heavyMachineGunShootingSpeed : float = 0.1;
var shootingDelta : float = 10;

var reactorHalo : ParticleSystem;
var reactorCore : ParticleSystem;
var reactorActivationEffect : GameObject;

var moveHorizontal : float = 0;
var moveVertical : float = 0;
var movement : Vector3;

function Start () 
{
	//rigidbody.transform.Rotate(0, 90, 270);
}

function Update () 
{	
	//DEPLACEMENTS--------------------------------------------------------------------
	//Horizontaux et Verticaux
	moveHorizontal = Input.GetAxis ("Horizontal");
    moveVertical = Input.GetAxis ("Vertical");

    movement = new Vector3 (0.0f, moveVertical, moveHorizontal);
    rigidbody.velocity = movement * speed;

    rigidbody.position = new Vector3 
    (
    	0.0f,
    	Mathf.Clamp (rigidbody.position.y, boundary.yMin, boundary.yMax),
        Mathf.Clamp (rigidbody.position.z, boundary.zMin, boundary.zMax)
    );
	
	//Rotations
    rigidbody.rotation = Quaternion.Euler (rigidbody.velocity.y * -tilt, 90, 270);

    //Reacteurs
    if(Input.GetButtonDown("Boost"))
	{
		Instantiate(reactorActivationEffect, Vector3(rigidbody.position.x, rigidbody.position.y, rigidbody.position.z - 1.5), Quaternion.Euler (0.0f, 90, 0.0f));
	}
    if(!Input.GetButton("Boost"))
    {
    	reactorCore.startColor = new Color32(128, 70, 46, 255);
	   	if(Input.GetAxis("Horizontal") > 0)
		{
			reactorHalo.startSpeed = 3;
		   	reactorHalo.emissionRate = 20;
		   	reactorHalo.maxParticles = 20;
		   	
		   	reactorCore.startSpeed = 3;
		   	reactorCore.emissionRate = 100;
		   	reactorCore.maxParticles = 100;
		   	
		}
		else if(Input.GetAxis("Horizontal") < 0)
		{
			reactorHalo.startSpeed = 0.1;
		   	reactorHalo.emissionRate = 5;
		   	reactorHalo.maxParticles = 20;
		   	
		   	reactorCore.startSpeed = 1;
		   	reactorCore.emissionRate = 10;
		   	reactorCore.maxParticles = 20;
		}
		else
		{
			reactorHalo.startSpeed = 1;
		   	reactorHalo.emissionRate = 30;
		   	reactorHalo.maxParticles = 20;
		   	
		   	reactorCore.startSpeed = 2;
		   	reactorCore.emissionRate = 50;
		   	reactorCore.maxParticles = 50;
		}
	}
	else
    {
    	moveHorizontal = Input.GetAxis("Boost");
    	movement = new Vector3 (0.0f, 0.0f, moveHorizontal);
    	
	    rigidbody.velocity = movement * (speed * 10);
	    rigidbody.position = new Vector3 
	    (
	    	0.0f,
	    	Mathf.Clamp (rigidbody.position.y, boundary.yMin, boundary.yMax),
	        Mathf.Clamp (rigidbody.position.z, boundary.zMin, boundary.zMax)
	    );
    	
    	reactorCore.startColor = new Color32(0, 149, 255, 255);
		reactorHalo.startSpeed = 2;
	   	reactorHalo.emissionRate = 10;
	   	reactorHalo.maxParticles = 10;
	   	
	   	reactorCore.startSpeed = 10;
	   	reactorCore.emissionRate = 300;
	   	reactorCore.maxParticles = 300;
	}
	//FDEPLACEMENTS-------------------------------------------------------------------


	//GESTION DES ARMES-------------------------------------------------------------------
	shootingDelta += Time.deltaTime;
	if(Input.GetButtonDown("Shoot") || Input.GetButton("Shoot"))
	{
		switch(weaponType)
		{
			case "defaultGun":
				if(shootingDelta >= defaultGunShootingSpeed)
				{
					Instantiate(bullet, bulletSpawnLeft.position, bulletSpawnLeft.rotation);
					Instantiate(bullet, bulletSpawnRight.position, bulletSpawnRight.rotation);
					shootingDelta = 0;
				}	
				break;
			default:
				break;
		}
	}
	//GESTION DES ARMES-------------------------------------------------------------------
	
	if(Input.GetButtonDown("Quit"))
		Application.Quit();//Faire pause
}

function OnTriggerEnter (other : Collider)
{
	if(other.tag == "Enemy")
	{
		Instantiate(explosion, transform.position, transform.rotation);
		Instantiate(restarter, transform.position, transform.rotation);
		Destroy(gameObject);
	}
}