﻿var bulletSpeed : float = 80;
var countDownTimer : float = 2;
var bulletType : String = "defaultGun";

function Start ()
{
	//rigidbody.velocity = transform.forward * bulletSpeed;
}

function Update () 
{
	transform.position.z += bulletSpeed * Time.deltaTime;
	countDownTimer -= Time.deltaTime;
	if(countDownTimer <= 0)
		Destroy(gameObject);
}

function OnTriggerEnter (other : Collider)
{
	if(other.tag == "Enemy")
		if(bulletType == "defaultGun")
			Destroy(gameObject);
}