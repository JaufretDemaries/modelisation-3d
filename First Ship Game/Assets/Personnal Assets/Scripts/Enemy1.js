﻿var moveSpeed : float = 8;
var killZPosition : float = -50;
var horizontalRandom : float = 10;
var explosion : GameObject;

function Start () 
{
	transform.position.y += Random.Range(horizontalRandom, -horizontalRandom);
}

function Update () 
{
	transform.position.z -= moveSpeed * Time.deltaTime;
	if(transform.position.z <= killZPosition)
		Destroy(gameObject);
}

function OnTriggerEnter (other : Collider)
{
	if(other.tag == "Bullet")
	{
		Instantiate(explosion, transform.position, transform.rotation);
		Destroy(gameObject);
	}
	if(other.tag == "Player")
	{
		Instantiate(explosion, transform.position, transform.rotation);
		Destroy(gameObject);
	}
}